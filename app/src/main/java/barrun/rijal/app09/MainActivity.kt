package barrun.rijal.app09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener , SeekBar.OnSeekBarChangeListener {

    val daftarLagu  = intArrayOf(R.raw.music_1,R.raw.music_2,R.raw.music_3)
    val davCov = intArrayOf(R.drawable.img1,R.drawable.img2,R.drawable.img3)
    lateinit var  db : SQLiteDatabase
    lateinit var adaplist : SimpleCursorAdapter
    var posLaguNow = 0
    var handler = Handler()
    lateinit var  mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController
    var id_mus  =0
    var id_covr = ""
    var judul = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        //seekbar
        seekSong.max=100
        seekSong.progress =0
        seekSong.setOnSeekBarChangeListener(this)
        //btn
        btnNext.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)

        //jalankan fun
        getMusik()
        listMusik()

        //list view
        lv.setOnItemClickListener(clik)



    }
    //read data base
    fun getMusik(): SQLiteDatabase {
        db= DbMusik(this).writableDatabase
        return db
    }

    //tampil list view
    fun listMusik(){
        var sql = "select id_musik as _id , id_cov , judul from musik"
        val c : Cursor = db.rawQuery(sql,null)
        adaplist = SimpleCursorAdapter(this,
            R.layout.list,
            c,
            arrayOf("_id","id_cov","judul"),
            intArrayOf(R.id.id_musik,R.id.id_cov,R.id.judul),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        lv.adapter = adaplist
    }

    //function seekbar
    fun miliSecondToString(ms:Int): String {
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        var menit = TimeUnit.SECONDS.toMinutes(detik)
        detik =detik % 60

        return "$menit : $detik"
    }

    fun audioPlayer(pos : Int){
        mediaPlayer = MediaPlayer.create(this,daftarLagu[pos])
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(miliSecondToString(seekSong.max))
        txCrTine.setText(miliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imV.setImageResource(davCov[pos])
        txJudulLagu.setText(judul)
        mediaPlayer.start()
        var updateSeek = UpdateSeekBarTread()
        handler.postDelayed(updateSeek,50)
    }

    fun audioNext(){
        if (mediaPlayer.isPlaying)mediaPlayer.stop()
        if (posLaguNow<(daftarLagu.size-1)){
            posLaguNow++
        }else{
            posLaguNow = 0
        }
        audioPlayer(posLaguNow)
    }

    fun audioPrev(){
        if (mediaPlayer.isPlaying)mediaPlayer.stop()
        if (posLaguNow >0){
            posLaguNow--
        }else{
            posLaguNow = daftarLagu.size-1
        }
        audioPlayer(posLaguNow)
    }

    fun audioStop(){
        if (mediaPlayer.isPlaying)mediaPlayer.stop()
    }


    inner class UpdateSeekBarTread : Runnable{
        override fun run() {
            var currTime  = mediaPlayer.currentPosition
            txCrTine.setText(miliSecondToString(currTime))
            seekSong.progress=currTime
            if(currTime != mediaPlayer.duration) handler.postDelayed(this,50)

        }

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay->{
                audioPlayer(posLaguNow)
            }
            R.id.btnNext->{
                audioNext()
            }
            R.id.btnPrev->{
                audioPrev()
            }
            R.id.btnStop->{
                audioStop()
            }
        }
    }

    //seekBar
    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let {mediaPlayer.seekTo(it)}
    }

    val clik = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        judul = c.getString(c.getColumnIndex("judul"))

        audioPlayer(c.position)

    }

}

