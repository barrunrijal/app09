package barrun.rijal.app09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DbMusik (context: Context): SQLiteOpenHelper(context,Db_name,null,Db_ver) {

    companion object{
        val Db_name = "musik"
        val Db_ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tblmus = "create table musik(id_musik Int primary key , id_cov Int not null, judul text)"
        val inslist = "insert into musik(id_musik,id_cov,judul) values ('0x7f0b0000','0x7f060057','Billie Eilish - No time to die'),('0x7f0b0001','0x7f060058','David Guetta'),('0x7f0b0002','0x7f060059','Remind me to forget')"

        db?.execSQL(tblmus)
        db?.execSQL(inslist)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}